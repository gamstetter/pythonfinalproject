from userSetup import *
from gameBoard import *
from compSetup import *

import time
import kivy
kivy.require('1.0.7')

from kivy.app import App

class PlayGame(App):
    def build(self):
        self.userBoard=UserBoard().run()
        self.compBoard=CompBoard()
        self.compBoard.setUpBoard()
        self.compScore=17
        self.userScore=17
        gameBoard=GridLayout(cols=20)
        for i in range(10):
            for j in range(10):
                if UserBoard[i][j]==None:
                    gameBoard.add_widget(Button(background_color=(0, 0, 130, 230), on_press=self.callback))
                else:
                    gameBoard.add_widget(Button(background_color=(1, 1, 1, 1), on_press=self.callback))
        for k in range(100):
            gameBoard.add_widget(Button(background_color=(0, 0, 130, 230)))
    
    def callback(self, value):
        row=int(value.pos[1]//54)
        if row==10:
            row=9
        col=int(value.pos[0]//144)
        if col==10:
            col=9
        row=9-row
        col=9-col
        if self.compBoard[row][col]==0:
            self.compBoard[row][col]=1
            self.userScore-=1
            value.bind(Line(circle=(150, 150, 50, 0, 359), closed=True, color=(255, 0, 0, 150)))
        else:
            self.compBoard[row][col]=-1
            value.bind(Line(circle=(150, 150, 50, 0, 359), closed=True, color=(255, 255, 255, 150)))
        compTurn()

    def compTurn(self):
        #find a random place where computer hasn't tried to hit yet
        x_coord=random.randint(0, self.size-1)
        y_coord=random.randint(0, self.size-1)
        if userBoard[x_coord][y_coord]==-1 or userBoard[x_coord][y_coord]==1:
            x_coord=random.randint(0, self.size-1)
            y_coord=random.randint(0, self.size-1)
        row=9-y_coord
        col=9-x_coord

        if userBoard[row][col]==None:
            userBoard[row][col]=-1
            
        else:
            userBoard[row][col]=1
            compScore-=1

if __name__ == '__main__':
    PlayGame().run()
