import random

class CompBoard():
    def __init__(self, size=10):
        self.size=size
        self.board=[[None]*size for _ in range(size)]
        self.shipSize=[5, 4, 3, 3, 2]

    def setUpBoard(self):
        for i in range(5):
            place_ship(shipSize[i])
        return self.board
    
    def place_ship(self, shipSize):
        direction=random.randint(1, 4) #1=up, 2=right, 3=down, 4=left

        #generate random location for ship placement
        x_coord=random.randint(0, self.size-1)
        y_coord=random.randint(0, self.size-1)

        #determine location where ship will fit on the board
        fits=False
        while not fits:
            if direction==1 and (x_coord-shipSize+1)>=0:
                for i in range(shipSize):
                    if self.board[x_coord][y_coord]!=None:
                        fits=False
                        break
                    fits=True
                    x_coord-=1
            elif direction==2 and (y_coord+shipSize-1)<=self.size-1:
                for i in range(shipSize):
                    if self.board[x_coord][y_coord]!=None:
                        fits=False
                        break
                    fits=True
                    y_coord+=1
            elif direction==3 and (x_coord+shipSize-1)<=self.size-1:
                for i in range(shipSize):
                    if self.board[x_coord][y_coord]!=None:
                        fits=False
                        break
                    fits=True
                    x_coord+=1
            elif direction==4 and (y_coord-shipSize+1)>=0:
                for i in range(shipSize):
                    if self.board[x_coord][y_coord]!=None:
                        fits=False
                        break
                    fits=True
                    y_coord-=1
            if not fits:
                direction=random.randint(1, 4)
                x_coord=random.randint(0, self.size-1)
                y_coord=random.randint(0, self.size-1)
        
        #update values on self.board
        #note x_coord/y_coord will increment opposite direction to counteract previous while loop
        for i in range(shipSize):
            if direction==1:
                self.board[x_coord][y_coord]=0
                x_coord+=1
            elif direction==2:
                self.board[x_coord][y_coord]=0
                y_coord-=1
            elif direction==3:
                self.board[x_coord][y_coord]=0
                x_coord-=1
            else:
                self.board[x_coord][y_coord]=0
                y_coord+=1


if __name__ == '__main__':
    CompBoard()
