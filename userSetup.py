import time
import kivy
kivy.require('1.0.7')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen

class UserBoard(App):

    def build(self, size=10):
        self.usboard=GridLayout(rows=size)
        self.count=17
        self.arr=[[None]*size for _ in range(size)]
        for i in range(size*size):
            self.usboard.add_widget(Button(background_color=(0, 0, 130, 230), on_press=self.callback))
        return self.usboard

    def callback(self, value):
        value.background_color=(1, 1, 1, 1)

        #FYI: Kivy calculates row col from bottom left, not top left
        row=int(value.pos[1]//54)
        if row==10:
            row=9
        col=int(value.pos[0]//72)
        if col==10:
            col=9
        row=9-row
        col=9-col
        #represent board in array format
        self.arr[row][col]=0

        #keep track of how many ship pieces are placed
        self.count-=1
        if self.count==0:
            time.sleep(1)
            self.usboard.stop()
            return self.arr
            
if __name__ == '__main__':
    UserBoard().run()
